Gunakan software standart untuk membuat schedule seperti microsoft project dan android studio

Item pekerjaan dalam schedule adalah item WBS pada level tertentu yang dianggap layak dan efektif untuk dimanage. Terlalu detail dan terlalu general kurang baik dilihat dari sisi manfaat schedule. Level WBS harus dianggap optimum dari sisi kegunaannya untuk dimanage.

Item schedule adalah item pekerjaan, bukan item yang lainnya. Gunakan istilah item pekerjaan yang gampang dipahami oleh pihak yang akan menggunakannya dan sesingkat mungkin.

Grouping disarankan sesuai dengan grouping yang ada pada WBS yang menjadi basis bagi proses perencanaan lainnya. Sehingga terjadi grouping yang seragam yang akan memudahkan proses perencanaan dan monitoring pada knowledge manajemen yang lainnya.

Memasukkan semua hari libur nasional maupun lokal setempat. Disamping itu juga mempertimbangkan penurunan produktifitas pada masa-masa tertentu seperti musim hujan.

Setting hubungan antara pekerjaan disarankan sesuai kaidah dan kondisi aktual serta cukup detil. Keterkaitan satu item pekerjaan dengan beberapa item pekerjaan lainnya sebaiknya selengkap mungkin untuk menjamin jika dilakukan proses simulasi, akan terlihat perubahan jalur kritis yang logis dan rasional.

Memasukkan durasi normal berdasarkan pengalaman pada tiap pekerjaan dengan mempertimbangkan ketersediaan resources dan metode pelaksanaan serta metode pengaturan tenaga kerja dan alat. Sampai pada tahap ini belum dimasukkan time contigency pada pekerjaan yang memiliki ketidakpastian tinggi.

Menampilkan jalur kritis dalam membuat schedule. Hal ini karena jalur kritis adalah informasi penting pada tiap schedule.

melihat kondisi apa yang ada pada di lingkungan masyarakat.

melakukan riset dengan masyarakat yang dibutuhkan apa saja dan melihat kondisi yang ada pada zaman sekarang.

melakukan kajian setelah ada rekomendasi dari masyarakat.
.
melakukan rapat besar apa yang harus dilakukan pada aplikasi EduCorner.

membagi tugas pada semua stakholder agar lebih cepat pengerjaannya.

setelah selesai pengerjaan tim melakukan simulasi pada aplikasi.

menguploud aplikasi di sistem yang ada.
 
melakukan iklan agar masyarakat tau

selesai